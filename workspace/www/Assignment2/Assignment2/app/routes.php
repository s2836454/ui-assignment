<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('job/showAll', array('as' => 'job.showAll', 'uses' => 'JobController@showAll'));
Route::get('user/showAllEmployers', array('as' => 'user.showAllEmployers', 'uses' => 'UserController@showAllEmployers'));
Route::get('user/documentation', array('as' => 'user.documentation', 'uses' => 'UserController@documentation'));

Route::resource('user', 'UserController');
Route::resource('job', 'JobController');

Route::get('/', function()
{
	return View::make('users.index');
});


/*
Route::get('about', function()
{
	return View::make('pages.about');
});
Route::get('projects', function()
{
	return View::make('pages.projects');
});
Route::get('contact', function()
{
	return View::make('pages.contact');
});*/

