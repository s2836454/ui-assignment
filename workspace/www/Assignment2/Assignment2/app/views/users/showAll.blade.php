@extends('layout')
@section('title')
Users
@stop
@section('content')
  <ul>
    @foreach ($users as $user)
      <li>{{{ $user->name }}}</li>
      <li>{{{ $user->userCategory }}}</li>
      <li>{{{ $user->email }}}</li>
      <li>{{{ $user->phone }}}</li><br>
    @endforeach
  </ul>
@stop