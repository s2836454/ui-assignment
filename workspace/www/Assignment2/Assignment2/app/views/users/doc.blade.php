<html>
  <body>
    <h1>Documentation</h1>
    <h2>Anthony Lee, s2836454, 3rd June 2014</h2>
    
    <p>Please view page source for information.</p>
    
    <!--

    Database Schema:

    Jobs:
        Schema::create('jobs', 
          function($table){
            $table->increments('id');
            $table->integer('user_id'); //foreign key for employer
            $table->string('title');
            $table->string('jobDescription');
            $table->string('location');
            $table->integer('salary');
            $table->date('startingDate');
            $table->date('endingDate');
            $table->timestamps();
          });

    Users:
        Schema::create('users', 
          function($table)
          {
            $table->increments('id');
            $table->integer('userCategory');
            $table->string('email')->unique();
            $table->string('password')->index();
            $table->string('name');
            $table->integer('phone');
            //$table->string('remember_token')->nullable();
            $table->timestamps();
          });

    Applications:
        Schema::create('applications', 
          function($table)
          {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('job_id');
            $table->string('applicationLetter');
            $table->date('applicationDate');
            $table->timestamps();
          });
    -->
    
    <!--
    No managers/user functionality, hence no usernames/passwords
    -->
    
    <!-- 
    Limitations

    Cannot Search for jobs
    No Pagination
    Expired jobs will still be shown
    Home page doesn't show recently added jobs
    No user registation/log in/log out
    No Image uploading or displaying
    Cannot apply for Jobs
    No job creation/updating/deleting
    No Validation
    No Sanitisation

    All pretty much a result of me not feeling confident with laravel and as a result, starting later than I should've.
  I only feel like I was truly grasping the concepts of routes two days before submission.

    -->
    
  </body> 
</html>