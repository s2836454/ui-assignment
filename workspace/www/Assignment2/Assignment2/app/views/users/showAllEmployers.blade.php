@extends('layout')
@section('title')
Users
@stop
@section('content')
  <table class="table table-striped table-bordered table-condensed">
    <tr>
      <th>Employer</th>
      <th>Description</th>
      <th>Phone No.</th>
    </tr>
    @foreach ($users as $user)
    <tr>
      <td>{{{$user->name}}}</td>
      <td>{{{$user->email}}}</td>
      <td>{{{$user->phone}}}</td>
    </tr>
    @endforeach   
@stop
    