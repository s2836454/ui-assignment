@extends('layout')
@section('title')
Users
@stop
@section('content')
  <table class="table table-striped table-bordered table-condensed">
    <tr>
      <th>Title</th>
      <th>Description</th>
      <th>Location</th>
      <th>Salary</th>
      <th>Starting Date</th>
      <th>Ending Date</th>
      <th>Days left to Apply</th>
    </tr>  
    <tr>
      <td>{{{ $job->title }}}</td>
      <td>{{{ $job->jobDescription }}}</td>
      <td>{{{ $job->location }}}</td>
      <td>{{{ $job->salary }}}</td>
      <td>{{{ $job->startingDate }}}</td>
      <td>{{{ $job->endingDate }}}</td>
      <td>{{{ $dateDiff  }}}</td>
    </tr>     
@stop
    
    
