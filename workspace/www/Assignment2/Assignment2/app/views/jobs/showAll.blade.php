@extends('layout')
@section('title')
Users
@stop
@section('content')
  <table class="table table-striped table-bordered table-condensed">
    <tr>
      <th>Title</th>
      <th>Description</th>
    </tr>
    @foreach ($jobs as $job)
    <tr>
      <td>{{ link_to_route('job.show', $job->title, array($job->id)) }}</td>
      <td>{{{ $job->jobDescription }}}</td>
    </tr>
    @endforeach
@stop
    