<html>
  <head>
    <title>@yield('title')</title>

    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">

    <!-- Optional theme -->
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css">

    <!-- Latest compiled and minified JavaScript -->
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>

  </head>
  <body>
    <p>Created by Anthony Lee, s2836454</p>
    <h1> Job Website </h1>

    <div>
      <div class="col-md-2">
        <div class=list-group-item>{{ link_to_route('user.index', 'User Home') }}</div>
        <div class=list-group-item>{{ link_to_route('job.showAll', 'List All Jobs') }}</div>
        <div class=list-group-item>{{ link_to_route('user.showAllEmployers', 'List All Employers') }}</div>
        <div class=list-group-item>{{ link_to_route('user.documentation', 'Documentation') }}</div>
        <!--@yield('links')-->
      </div>
      <div class="col-md-10">@yield('content')</div>
    </div>


  </body>
</html>