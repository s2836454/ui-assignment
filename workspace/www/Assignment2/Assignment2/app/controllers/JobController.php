<?php

class JobController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}
  
  /**
  * Returns a list of all jobs
  */
  public function showAll()
  {
     $jobs = Job::all();
     return View::make('jobs.showAll', compact('jobs'));
  }


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$job = Job::find($id);

    $now_date = new DateTime();
    $now_date->setTimestamp(strtotime(date('Y-m-d')));
    $finalDate = new DateTime();
    $finalDate->setTimestamp(strtotime(str_replace('-','/', $job->endingDate)));

    $dateDiff = $now_date->diff($finalDate)->format("%r%a");
    if ($dateDiff < 0) $dateDiff = 0;
    
    return View::make('jobs.show')->with('job', $job)->with('dateDiff', $dateDiff);
    
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
