<?php

class JobsTableSeeder extends Seeder {
 public function run()
 {
   $job = Job::create(array('title'=>'Toilet Cleaner',
                            'user_id'=>'1',
                            'jobDescription'=>'Employee will spend the day cleaning toilets and debugging Laravel',
                            'location'=>'Griffith University Gold Coast',
                            'salary'=>'12000',
                            'startingDate'=>'2014-5-20',
                            'endingDate'=>'2014-6-21',
                         ));
   
   $job = Job::create(array('title'=>'Movie Directer',
                            'user_id'=>'1',
                            'jobDescription'=>'Employee will rdirect movies',
                            'location'=>'Hollywood',
                            'salary'=>'80000',
                            'startingDate'=>'2014-8-1',
                            'endingDate'=>'2014-7-11',
                           ));
   
   $job = Job::create(array('title'=>'Butcher',
                            'user_id'=>'1',
                            'jobDescription'=>'Employee required to butcher things',
                            'location'=>'Butchers at Carrarra markets',
                            'salary'=>'50000',
                            'startingDate'=>'2014-9-30',
                            'endingDate'=>'2014-10-23',
                           ));
  }
}