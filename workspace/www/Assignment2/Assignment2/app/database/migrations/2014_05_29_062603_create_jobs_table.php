<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('jobs', 
      function($table){
        $table->increments('id');
        $table->integer('user_id'); //foreign key for employer
        $table->string('title');
        $table->string('jobDescription');
        $table->string('location');
        $table->integer('salary');
        $table->dateTime('startingDate');
        $table->dateTime('endingDate');
        $table->timestamps();
      });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
    Schema::drop('jobs');
	}

}
